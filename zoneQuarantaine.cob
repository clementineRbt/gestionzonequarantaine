IDENTIFICATION DIVISION.
PROGRAM-ID. exFicSEQbis.
ENVIRONMENT DIVISION.
CONFIGURATION SECTION.
SPECIAL-NAMES.
DECIMAL-POINT IS COMMA.
INPUT-OUTPUT SECTION.

FILE-CONTROL.

*> Association des fichiers séquentiels

SELECT fpatientsSortants ASSIGN TO "patientsSortants.dat"
ORGANIZATION SEQUENTIAL
ACCESS IS SEQUENTIAL
FILE STATUS IS fpatientsSortants_CR.

*> Association des fichiers indexés

SELECT fpatients ASSIGN TO "patients.dat"
ORGANIZATION indexed
ACCESS IS dynamic
RECORD KEY fpat_id
ALTERNATE RECORD KEY  fpat_docteurNum WITH DUPLICATES
ALTERNATE RECORD KEY  fpat_maladieNum WITH DUPLICATES
ALTERNATE RECORD KEY  fpat_batimentNum WITH DUPLICATES
FILE STATUS IS fpatients_CR.

SELECT fmaladies ASSIGN TO "maladies.dat"
ORGANIZATION indexed
ACCESS IS dynamic
RECORD KEY fmal_num
ALTERNATE RECORD KEY  fmal_nbGueris WITH DUPLICATES
ALTERNATE RECORD KEY  fmal_nbMorts WITH DUPLICATES
FILE STATUS IS fmaladies_CR.

SELECT fdocteurs ASSIGN TO "docteurs.dat" 
ORGANIZATION indexed 
ACCESS IS DYNAMIC
RECORD KEY fdoc_id
ALTERNATE RECORD KEY  fdoc_nom
FILE STATUS IS fdocteurs_CR.

SELECT fbatiments ASSIGN TO "batiments.dat" 
ORGANIZATION indexed 
ACCESS IS DYNAMIC
RECORD KEY fbat_num
ALTERNATE RECORD KEY fbat_droitAcces WITH DUPLICATES
FILE STATUS IS fbatiments_CR.

DATA DIVISION. 

*> Declaration des attributs

FILE SECTION.

FD fdocteurs.
01 docteur_tamp.
02 fdoc_id PIC 9(6).
02 fdoc_nom PIC A(30).
02 fdoc_mdp PIC A(30).
02 fdoc_prenom PIC A(30).
02 fdoc_secteur PIC A(30).
02 fdoc_rangResp PIC 9(2).

FD fmaladies.
01 maladie_tamp.
02 fmal_num PIC 9(6).
02 fmal_nom PIC A(30).
02 fmal_nbMorts PIC 9(7).
02 fmal_nbGueris PIC 9(7).
02 fmal_tauxT PIC 9(3)V99.

FD fpatients.
01 patient_tamp.
02 fpat_id PIC 9(12).
02 fpat_nom PIC A(30).
02 fpat_prenom PIC A(30).
02 fpat_docteurNum PIC 9(6).
02 fpat_batimentNum PIC 9(4).
02 fpat_chambre PIC 9(4).
02 fpat_maladieNum PIC 9(6).
02 fpat_niveau PIC A(30).

FD fbatiments.
01 batiment_tamp.
02 fbat_num PIC 9(4).
02 fbat_responsable PIC 9(6).
02 fbat_nbPlacesLibres PIC 9(5).
02 fbat_capacite PIC 9(5).
02 fbat_maladieNum PIC 9(6).
02 fbat_droitAcces PIC 9(2).

FD fpatientsSortants.
01 patientsSortants_tamp.
02 fpatsort_num PIC 9(4).
02 fpatsort_etat PIC A(8).
02 fpartsort_maladieNum PIC 9(6).


WORKING-STORAGE SECTION.
77 fdocteurs_CR PIC X(2). 
77 fmaladies_CR PIC X(2). 
77 fpatients_CR PIC X(2). 
77 fbatiments_CR PIC X(2).
77 fpatientsSortants_CR PIC X(2).
77 WMenu PIC 9.
77 Wrep PIC 9.
77 Wfin PIC 9.
77 Wfin2 PIC 9.
77 Wtrouve PIC 9.
77 WnumMaladie PIC 9(6).
77 Wnumero PIC 9(6).
77 trouve PIC X(2).
77 finFichier PIC 9(2).
77 finZone PIC 9(2).
77 trouve2 PIC 9(2).
77 existe PIC 9(2).
77 wEtat PIC A(6).
77 nbMalades PIC 9(6).
77 WcleExiste PIC 9.
77 WmalExiste PIC 9.
77 WnumBat PIC 9(4).
77 WnumBat2 PIC 9(4).
77 WnbrPatient PIC 9(5).
77 WnewBatPatient PIC 9(4).
77 WoptBat PIC 9.
77 Wbool PIC 9.
77 WtotalMaladesSortants PIC 9(3).
77 WpourcentageGueris PIC 9(3)V99.
77 WpourcentageMorts PIC 9(3)V99.
77 Wchoix PIC 9.
77 min PIC 9.
77 max PIC 9.
77 filou PIC X(2).
77 WnomMaladie PIC A(30).
77 WnbGueris PIC 9(7).
77 WnbMorts PIC 9(7).
77 WtauxTransmission PIC 9(3)V99.
77 estLoguer PIC 9(2).
*>Utile pour les tests menu
77 WidMedecin PIC 9(6).
77 WnumMedecin PIC 9(6).
77 WmdpMedecin PIC A(30).
77 WidPatient PIC 9(12).
77 WdocExiste PIC 9.
77 WdocValide PIC 9.
77 WidDoc PIC 9(6).
77 WbatExiste PIC 9.
77 WmaladieBat PIC 9(6).
77 WtauxOccupationBat PIC 9(3)V99.
77 WafficherToutesMaladies PIC 9.



PROCEDURE DIVISION.
*> Modifier l'id par l'id de connection, la variable globale sera utile pour la suite des fonctions appellées dans le menu
PERFORM WITH TEST AFTER UNTIL estLoguer = 1
       *>Création d'un compte admin
       OPEN INPUT fdocteurs
       IF fdocteurs_CR =35  THEN
           CLOSE fdocteurs
           OPEN OUTPUT fdocteurs
               MOVE 0 TO fdoc_id
               MOVE "admin" TO fdoc_nom
               MOVE "unMDP" TO fdoc_mdp
               MOVE 10 TO fdoc_rangResp
               WRITE docteur_tamp END-WRITE
           CLOSE fdocteurs
           OPEN INPUT fdocteurs
       END-IF
       DISPLAY "***** MENU DE CONNECTION *****"
       DISPLAY "Veuillez saisir votre identifiant"
       ACCEPT WidMedecin
       DISPLAY "Veuillez saisir votre mot de passe"
       ACCEPT WmdpMedecin
       
           MOVE WidMedecin TO fdoc_id
           READ fdocteurs
               INVALID KEY
                   DISPLAY "Erreur de connection, veuillez recommencer"
                   MOVE 0 TO estLoguer
               NOT INVALID KEY
                   IF WmdpMedecin = fdoc_mdp THEN
                       DISPLAY "Bonjour ",fdoc_nom
                       DISPLAY "------------------"
                       MOVE 1 TO estLoguer
                   ELSE
                       DISPLAY "Erreur de connection, veuillez recommencer"
                       MOVE 0 TO estLoguer
                   END-IF
           END-READ 
       CLOSE fdocteurs
END-PERFORM

*> Mettre la condition pour arriver au menu
PERFORM WITH TEST AFTER UNTIL Wmenu = 0
       MOVE 0 TO WMenu
       DISPLAY "********* MENU PRINCIPAL **********"
       DISPLAY '1) Consulter vos patients'
       DISPLAY '2) Ajouter un patient'
       DISPLAY '3) Modifier un patient'
       DISPLAY "4) Effectuer procédure de sortie d'un patient"
       DISPLAY "5) Consulter maladies et statistiques"
       DISPLAY "6) Modifier un bâtiment"
       DISPLAY "7) Supprimer un bâtiment"
       DISPLAY "8) Afficher la liste des patients sortis de la zone"
    
       OPEN INPUT fdocteurs
           
       MOVE WidMedecin TO fdoc_id
       READ fdocteurs
       INVALID KEY 
           *> Pas affiché
           DISPLAY'Pas de docteur'
       NOT INVALID KEY
           IF fdoc_rangResp > 7 THEN
               DISPLAY "9) Ajouter un docteur"
               DISPLAY "10) Modifier un docteur"
               DISPLAY "11) Supprimer un docteur"
           END-IF

       END-READ 

       CLOSE fdocteurs

       DISPLAY "0) Quitter"

       ACCEPT Wmenu

       IF Wmenu = 1 THEN
           PERFORM AFFICHER_PATIENTS
           ELSE IF Wmenu = 2 THEN
               PERFORM AJOUTER_PATIENT
               ELSE IF Wmenu = 3 THEN
                   PERFORM MODIFIER_PATIENT
                   ELSE IF Wmenu = 4 THEN
                       PERFORM SUPPRIMER_PATIENT
                       ELSE IF Wmenu = 5 THEN
                           PERFORM AFFICHER_MALADIES
                           ELSE IF Wmenu = 6 THEN
                               PERFORM MODIFIER_BATIMENT
                               ELSE IF Wmenu = 7 THEN
                                   PERFORM SUPPRIMER_BATIMENT
                                   ELSE IF Wmenu = 8 THEN
                                       PERFORM AFFICHER_PATIENTS_SORTANTS
                                       ELSE IF Wmenu = 9 AND fdoc_rangResp > 7 THEN
                                           PERFORM AJOUTER_DOCTEUR
                                           ELSE IF Wmenu = 10 AND fdoc_rangResp > 7 THEN
                                               PERFORM MODIFIER_DOCTEUR
                                               ELSE IF Wmenu = 11 AND fdoc_rangResp > 7 THEN
                                                   PERFORM SUPPRIMER_DOCTEUR
                                               END-IF
                                           END-IF
                                       END-IF
                                   END-IF
                               END-IF
                           END-IF
                       END-IF
                   END-IF
               END-IF
           END-IF
       END-IF
           

       


END-PERFORM

*>PERFORM MENU1

STOP RUN.

AJOUTER_PATIENT.
       OPEN I-O fpatients
       DISPLAY fpatients_CR
       IF fpatients_CR = 35 THEN
           OPEN OUTPUT fpatients
           CLOSE fpatients
           OPEN I-O fpatients
       END-IF
       PERFORM WITH TEST AFTER UNTIL Wrep = 0
           IF fpatients_CR = 00 THEN
               DISPLAY 'Donnez les informations relatives au patient'
               PERFORM WITH TEST AFTER UNTIL existe = 0
                   DISPLAY 'Numéro du patient :'
                   ACCEPT fpat_id
                   PERFORM VERIFIER_PATIENT
               END-PERFORM
               DISPLAY 'Nom du patient : '
               ACCEPT fpat_nom
               DISPLAY 'Prénom du patient : '
               ACCEPT fpat_prenom
       *>Ajout securiser d'un docteur
               PERFORM VERIFIER_FPAT_DOCTEURNUM
               *>MOVE WnumMedecin TO fpat_docteurNum
       *>FIN
       *>Ajout securiser d'une maladie
               PERFORM VERIFIER_FPAT_MALADIENUM
               *>MOVE WnumMaladie TO fpat_maladieNum
       *>FIN               
               DISPLAY 'Niveau d''avancement de la maladie :'
               ACCEPT fpat_niveau
       *>Ajout securiser d'un batiment               
               PERFORM VERIFIER_FPAT_FBATNUM
               *>MOVE WnumBat TO fpat_batimentNum
       *>FIN   
               
               DISPLAY 'Numéro de la chambre :'
               ACCEPT fpat_chambre   
               CLOSE fpatients
               OPEN I-O fpatients
               PERFORM AFFICHER_UN_PATIENT
               WRITE patient_tamp END-WRITE
               PERFORM WITH TEST AFTER UNTIL Wrep = 0 OR Wrep = 1
                   DISPLAY 'Voulez vous ajouter un autre patient? 1 ou 0'
                   ACCEPT Wrep
               END-PERFORM
           END-IF
       END-PERFORM
       CLOSE fpatients.

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>>Permet de vérifier si un patient existe, la variable existe prend 1 si le patient existe
VERIFIER_PATIENT. *>ATTENTION : pour untiliser cette procédure vous devez ouvrir le fichier fpatients en lecture ou maj
       READ fpatients RECORD KEY IS fpat_id
           INVALID KEY
               MOVE 0 TO existe
           NOT INVALID KEY
               DISPLAY "Ce patient existe déjà"
               MOVE 1 TO existe
       END-READ      
.

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Permet d'ajouter un docteur dans fdocteurs
AJOUTER_DOCTEUR.
       OPEN I-O fdocteurs
       DISPLAY fdocteurs_CR
       IF fdocteurs_CR = 35 THEN
           OPEN OUTPUT fdocteurs
       END-IF
       PERFORM WITH TEST AFTER UNTIL Wrep = 0
           IF fdocteurs_CR= 00 THEN
               DISPLAY 'Donnez les informations relatives au docteur'
               DISPLAY 'Numéro du docteur :'
               ACCEPT fdoc_id
               DISPLAY 'Nom du docteur : '
               ACCEPT fdoc_nom
               DISPLAY 'Mot de passe du docteur'
               ACCEPT fdoc_mdp
               DISPLAY 'Prénom du docteur : '
               ACCEPT fdoc_prenom
               DISPLAY 'Precisez le secteur d''expertise du docteur'
               ACCEPT fdoc_secteur
               DISPLAY 'Donnez le rang de reponsabilité du docteur'
               ACCEPT fdoc_rangResp
               WRITE docteur_tamp END-WRITE
               PERFORM WITH TEST AFTER UNTIL Wrep = 0 OR Wrep = 1
                   DISPLAY 'Voulez vous ajouter un autre docteur? 1 ou 0'
                   ACCEPT Wrep
               END-PERFORM
           END-IF
       END-PERFORM
       CLOSE fdocteurs.

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>Permet d'ajouter un nouveau batiment dans fbatiments
AJOUTER_BATIMENT.

OPEN I-O fbatiments
DISPLAY fbatiments_CR
IF fbatiments_CR = 35 THEN
    OPEN OUTPUT fbatiments
    CLOSE fbatiments
    OPEN I-O fbatiments
END-IF

PERFORM WITH TEST AFTER UNTIL Wrep = 0
    MOVE 1 TO WcleExiste
    DISPLAY 'Veuillez renseigner les informations sur le batiments à ajouter.'
    PERFORM WITH TEST AFTER UNTIL WcleExiste = 0
        DISPLAY 'Numéro du batiment : '
        ACCEPT fbat_num
        READ fbatiments
            INVALID KEY 
                MOVE 0 to WcleExiste
                DISPLAY 'Veuillez renseigner les autres informations sur ce nouveau batiment.'
            NOT INVALID KEY
                MOVE 1 TO WcleExiste
                DISPLAY 'Ce batiment existe déjà.'
                DISPLAY 'Voici la liste des batiments déjà existant : '
                CLOSE fbatiments
                PERFORM AFFICHER_BATIMENTS
                OPEN INPUT fbatiments
                DISPLAY 'Veuillez saisir un numero de batiment non inclus dans cette liste. '
        END-READ
       END-PERFORM

    *> Saisie du docteur responsable du batiment 
    MOVE 1 TO WdocValide
    PERFORM WITH TEST AFTER UNTIL WdocValide = 0  
        *> Vérification sur la saisie 
        *> Il faut aussi vérifier que le docteur est un rang supérieur à 7 pour être responsable
            DISPLAY 'Identifiant du docteurs responsable du batiment (attention le rang dans être supérieur à 7):'
            ACCEPT WdocExiste
        OPEN INPUT fdocteurs
        MOVE WdocExiste TO fdoc_id

        READ fdocteurs
        INVALID KEY
            DISPLAY 'Veuillez de nouveau saisir le numéro du docteur parmie la liste suivante : '
            CLOSE fdocteurs
            PERFORM LISTE_RANG_SEPT
            OPEN INPUT fdocteurs
        NOT INVALID KEY
            IF fdoc_rangResp > 6 THEN 
                MOVE 0 TO WdocValide
                MOVE WdocExiste TO fbat_responsable
            ELSE 
                DISPLAY 'Le rang de ce docteur n est pas accès élevé (rang 7 minimum).'
                CLOSE fdocteurs
                PERFORM LISTE_RANG_SEPT
                OPEN INPUT fdocteurs
                DISPLAY 'Veuilez saisir un identifiant de docteur présent dans la liste '
        END-READ
        CLOSE fdocteurs
    END-PERFORM
    *> On considère que quand un batiment est créer il n'y a pas 
    *>encore de patient à l'intérieur  
    DISPLAY 'Capacité d accueil du batiment (nb de places) :'
    ACCEPT fbat_nbPlacesLibres
    MOVE fbat_nbPlacesLibres TO fbat_capacite

    *>  Saisie de la maladie et Vérification
    *> Fonction qui valide la saisie de la maladie
    PERFORM VERIFIER_FPAT_MALADIENUM
    MOVE WnumMaladie TO fbat_maladieNum

    DISPLAY 'Rang inférieur du docteur qui a accès au batiment : '
    *> Vérification que le rang soit bien compris entre le rang minimum et maximum (0 et 9) 
    MOVE 0 TO min
    MOVE 9 TO max
    PERFORM SAISIR_ENTRE
    MOVE Wchoix TO fbat_droitAcces

    *> Pour stocker le numero du batiment pour le Supprimer batiment 
    *> Quand il faut déplacer des patients d'un batiment à un nouveau
    MOVE fbat_num TO WnewBatPatient

    WRITE batiment_tamp
    END-WRITE
    PERFORM WITH TEST AFTER UNTIL Wrep = 0 OR Wrep = 1
        DISPLAY 'Voulez vous ajouter un autre batiment ? 1 ou 0'
        ACCEPT Wrep
    END-PERFORM
END-PERFORM

CLOSE fbatiments.

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>Permet l'ajout d'une maladie dans fmaladies
AJOUTER_MALADIE.
PERFORM WITH TEST AFTER UNTIL Wrep = 0
           OPEN I-O fmaladies
           DISPLAY fmaladies_CR
           IF fmaladies_CR = 35 THEN
               OPEN OUTPUT fmaladies
           END-IF
           IF fmaladies_CR= 00 THEN
               DISPLAY 'Donnez les informations relatives à la maladie'
               DISPLAY 'Numéro de la maladie :'
               ACCEPT fmal_num
               DISPLAY 'Nom de la maladie : '
               ACCEPT fmal_nom
               DISPLAY 'Taux de transmission :'
               ACCEPT fmal_tauxT
               MOVE 0 TO fmal_nbMorts
               MOVE 0 TO fmal_nbGueris
               WRITE maladie_tamp 
               INVALID KEY 
               DISPLAY "Ce numéro de maladie existe déjà"
               NOT INVALID KEY
                   PERFORM WITH TEST AFTER UNTIL Wrep = 0 OR Wrep = 1
                       DISPLAY 'Voulez vous ajouter une autre maladie ? 1 ou 0'
                       ACCEPT Wrep
                       CLOSE fmaladies
                   END-PERFORM
           END-IF
       END-PERFORM.

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>Permet d'afficher les informations de tous les patients
AFFICHER_PATIENTS.
       OPEN INPUT fpatients
       MOVE 0 TO Wfin
       MOVE 0 TO trouve2
       IF fpatients_CR = 35 THEN
           MOVE 1 TO Wfin
       END-IF
       *> Pour savoir si le fichier est fini (plus aucune lecture possible)
       
       PERFORM WITH TEST AFTER UNTIL Wfin = 1

           READ fpatients NEXT
               AT END 
                   MOVE 1 TO Wfin
               NOT AT END
                   IF WidMedecin = fpat_docteurNum THEN
                       DISPLAY 'Numéro : ', fpat_id
                       DISPLAY 'Nom : ', fpat_nom
                       DISPLAY 'Prénom : ', fpat_prenom
                       DISPLAY 'Docteur : ', fpat_docteurNum
                       DISPLAY 'Batiment : ', fpat_batimentNum
                       DISPLAY 'Chambre : ', fpat_chambre
                       DISPLAY 'Maladie : ', fpat_maladieNum
                       DISPLAY 'Niveau : ', fpat_niveau
                       DISPLAY '-----------------'
                       MOVE 1 TO trouve2
                   END-IF
           END-READ
       END-PERFORM
       IF trouve2 = 0 THEN
           DISPLAY "Vous n'avez aucun patient"
       END-IF
       CLOSE fpatients.

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>Permet d'afficher toutes les informations d'un patient
AFFICHER_UN_PATIENT. *>UTILISATION : Vuillez renseigner fpat_id avant utilisation
       DISPLAY "Voici les informations actuelles du patient"
       DISPLAY '-----------------'
       DISPLAY 'Numéro : ', fpat_id
       DISPLAY 'Nom : ', fpat_nom
       DISPLAY 'Prénom : ', fpat_prenom
       DISPLAY 'Docteur : ', fpat_docteurNum
       DISPLAY 'Batiment : ', fpat_batimentNum
       DISPLAY 'Chambre : ', fpat_chambre
       DISPLAY 'Maladie : ', fpat_maladieNum
       DISPLAY 'Niveau : ', fpat_niveau
       DISPLAY "-------------------------------------------"
.

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>Permet d'afficher les informations de tous les docteurs 
AFFICHER_DOCTEURS.
       OPEN INPUT fdocteurs
       DISPLAY fdocteurs_CR
       *> Pour savoir si le fichier est fini (plus aucune lecture possible)    
       MOVE 0 TO Wfin
       PERFORM WITH TEST AFTER UNTIL Wfin = 1
           READ fdocteurs NEXT
               AT END 
                   MOVE 1 TO Wfin
               NOT AT END
                   DISPLAY 'Numéro : ', fdoc_id
                   DISPLAY 'Nom : ', fdoc_nom
                   DISPLAY 'Prénom : ', fdoc_prenom
                   DISPLAY 'Secteur : ', fdoc_secteur
                   DISPLAY 'Rang : ', fdoc_rangResp
                   DISPLAY '-----------------'
       END-PERFORM
       CLOSE fdocteurs.


*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Permet d'afficher toutes les informations d'un docteur
AFFICHER_UN_DOCTEUR. *>UTILISATION : veuillez renseigner fdoc_id avant utilisation
       DISPLAY 'Voici les informations actuels du docteur'
       DISPLAY '-----------------'
       DISPLAY 'Numéro : ', fdoc_id
       DISPLAY 'Nom : ', fdoc_nom
       DISPLAY 'Prénom : ', fdoc_prenom
       DISPLAY 'Secteur : ', fdoc_secteur
       DISPLAY 'Rang : ', fdoc_rangResp
       DISPLAY '-----------------'
.

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Permet d'afficher les informations de tous les batiments
AFFICHER_BATIMENTS.
OPEN INPUT fbatiments
DISPLAY fbatiments_CR
IF fbatiments_CR = 35 THEN
       OPEN OUTPUT fbatiments
       CLOSE fbatiments
       OPEN INPUT fbatiments
END-IF
MOVE 0 TO Wfin
PERFORM WITH TEST AFTER UNTIL Wfin = 1
    READ fbatiments NEXT
        AT END 
            MOVE 1 TO Wfin
        NOT AT END 
            DISPLAY 'Numéro du batiment : ', fbat_num
            DISPLAY 'Numéro du responsable : ', fbat_responsable
            DISPLAY 'Numéro de la maladie prise en charge : ', fbat_maladieNum
            DISPLAY 'Capacité d accueil : ', fbat_capacite
            DISPLAY 'Nombre de places de libres : ', fbat_nbPlacesLibres
            DISPLAY 'Grade inférieur d accès au batiment : ', fbat_droitAcces
            *> Calcul du taux d'occupation du batiment 
            COMPUTE WnbrPatient = fbat_capacite - fbat_nbPlacesLibres
            DIVIDE WnbrPatient BY fbat_capacite GIVING WtauxOccupationBat
            MULTIPLY WtauxOccupationBat BY 100 GIVING WtauxOccupationBat
            DISPLAY 'Taux d occupation du batiment : ', WtauxOccupationBat, ' %'
            DISPLAY '-----------------'
    END-READ
END-PERFORM
CLOSE fbatiments.

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Permet d'afficher les informations de toutes les maladies OU d'afficher les informations d'une maladies
AFFICHER_MALADIES.
OPEN INPUT fmaladies
OPEN INPUT fpatients
IF fmaladies_CR = 35 THEN
       DISPLAY "Aucune maladie"  
       OPEN OUTPUT fmaladies
       CLOSE fmaladies
       OPEN INPUT fmaladies
END-IF
IF fpatients_CR = 35 THEN
       OPEN OUTPUT fpatients
       CLOSE fpatients
       OPEN INPUT fpatients
END-IF


DISPLAY fmaladies_CR
MOVE 0 TO Wfin
MOVE 0 TO nbMalades


PERFORM WITH TEST AFTER UNTIL Wrep = 0 OR Wrep = 1
       DISPLAY "Voulez-vous voir le nombre de malade pour chaque maladie ou pour une maladie spécifique ? 0 ou 1"
       ACCEPT Wrep
END-PERFORM


IF Wrep = 0  THEN
PERFORM WITH TEST AFTER UNTIL Wfin = 1
READ fmaladies NEXT
AT END 
       DISPLAY "YO"
       MOVE 1 TO Wfin
NOT AT END

       *> Permet d'éviter la redondance de code entre affichage de toutes les maladies et affichage d'une seule maladie
       MOVE fmal_num TO WnumMaladie
       MOVE fmal_nom TO WnomMaladie
       MOVE fmal_nbGueris TO WnbGueris
       MOVE fmal_nbMorts TO WnbMorts
       MOVE fmal_tauxT TO WtauxTransmission
       PERFORM AFFICHAGE_MALADIE
    
       PERFORM CALCULS_POURCENTAGES

       *>Se Positionner
               MOVE fmal_num TO fpat_maladieNum
               START fpatients, KEY IS=fpat_maladieNum
               INVALID KEY
                   DISPLAY"Aucun patient n'a de ", fmal_nom
               NOT INVALID KEY
                   MOVE 0 TO Wfin2
                   MOVE 0 TO nbMalades
                   PERFORM WITH TEST AFTER UNTIL Wfin2 = 1 
                   *> Lire Suiv
                       READ fpatients NEXT
                       AT END
                           MOVE 1 TO Wfin2
                       NOT AT END
                           ADD 1 TO nbMalades
                           IF fpat_maladieNum IS NOT EQUAL TO fmal_num THEN 
                           SUBTRACT 1 FROM nbMalades
                           END-IF
                       END-READ
                       
                    END-PERFORM
                    DISPLAY 'Nombre de patients touchés : ', nbMalades

                      
               END-START
               
               DISPLAY '-----------------'
       
END-READ    

END-PERFORM
ELSE 
*> Voir tous les malades d'une maladie
DISPLAY "Numéro de la maladie que vous recherchez"
ACCEPT WnumMaladie
MOVE WnumMaladie TO fmal_num
READ fmaladies
INVALID KEY
      DISPLAY"Cette maladie n'existe pas"
NOT INVALID KEY

       MOVE fmal_num TO WnumMaladie
       MOVE fmal_nom TO WnomMaladie
       MOVE fmal_nbGueris TO WnbGueris
       MOVE fmal_nbMorts TO WnbMorts
       MOVE fmal_tauxT TO WtauxTransmission
       PERFORM AFFICHAGE_MALADIE

       PERFORM CALCULS_POURCENTAGES
       
       *>Se Positionner
               MOVE WnumMaladie TO fpat_maladieNum
               START fpatients, KEY IS=fpat_maladieNum
               INVALID KEY
                   DISPLAY"Aucun malade n'a cette maladie  "
               NOT INVALID KEY
                   MOVE 0 TO Wfin2
                   MOVE 0 TO nbMalades
                   PERFORM WITH TEST AFTER UNTIL Wfin2 = 1 OR fpat_maladieNum > fmal_num OR fpat_maladieNum < fmal_num
                   *> Lire Suiv
                       READ fpatients NEXT
                       AT END
                           MOVE 1 TO Wfin2
                       NOT AT END
                           ADD 1 TO nbMalades
                        END-READ
                    END-PERFORM
               
               DISPLAY "La maladie ", fmal_nom, " touche ", nbMalades, " patients."
       

END-IF

CLOSE fpatients
CLOSE fmaladies.


*> Fonctions pour éliminer la redondance de code : 
AFFICHAGE_MALADIE.

       DISPLAY 'Numéro : ', WnumMaladie
       DISPLAY 'Nom : ', WnomMaladie
       DISPLAY 'Nombre de guéris : ', WnbGueris
       DISPLAY 'Nombre de morts : ', WnbMorts
       DISPLAY 'Taux de transmission : ', WtauxTransmission, '%'.


CALCULS_POURCENTAGES.

ADD fmal_nbGueris fmal_nbMorts TO WtotalMaladesSortants

*> Calcul pourcentage de guéris
DIVIDE fmal_nbGueris BY WtotalMaladesSortants GIVING WpourcentageGueris
MULTIPLY WpourcentageGueris BY 100 GIVING WpourcentageGueris
DISPLAY"Taux de guérison : ", WpourcentageGueris, "%"

*> Calcul pourcentage de morts
DIVIDE fmal_nbMorts BY WtotalMaladesSortants GIVING WpourcentageMorts
MULTIPLY WpourcentageMorts BY 100 GIVING WpourcentageMorts
DISPLAY"Taux de mortalité : ", WpourcentageMorts, "%".


*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Permet d'afficher le nombre de malades par maladie
AFFICHER_NBMALADES_PAR_MALADIE.
OPEN INPUT fmaladies
OPEN INPUT fpatients
MOVE 0 TO nbMalades

PERFORM WITH TEST AFTER UNTIL Wrep = 0 OR Wrep = 1
       DISPLAY "Voulez-vous voir le nombre de malade pour chaque maladie ou pour une maladie spécifique ? 0 ou 1"
       ACCEPT Wrep
END-PERFORM

IF Wrep = 0 THEN
MOVE 0 TO Wfin
*> Voir tous les malades de chaque maladies
PERFORM WITH TEST AFTER UNTIL Wfin = 1
       READ fmaladies NEXT
       AT END  
              MOVE 1 TO Wfin
              
       NOT AT END
       
               *>Se Positionner
               MOVE fmal_num TO fpat_maladieNum
               START fpatients, KEY IS=fpat_maladieNum
               INVALID KEY
                   DISPLAY"Aucun malade n'a la maladie suivante :  ", fmal_nom
               NOT INVALID KEY
                   MOVE 0 TO Wfin2
                   MOVE 0 TO nbMalades
                   PERFORM WITH TEST AFTER UNTIL Wfin2 = 1 
                   *> Lire Suiv
                       READ fpatients NEXT
                       AT END
                           MOVE 1 TO Wfin2
                       NOT AT END
                           ADD 1 TO nbMalades
                           IF fpat_maladieNum IS NOT EQUAL TO fmal_num THEN 
                           SUBTRACT 1 FROM nbMalades
                           END-IF
                       END-READ
                    END-PERFORM
               
               DISPLAY "La maladie ", fmal_nom, " touche ", nbMalades, " patients."
               END-START
               
        END-READ
END-PERFORM   
ELSE 
*> Voir tous les malades d'une maladie
DISPLAY "Numéro de la maladie que vous recherchez"
ACCEPT WnumMaladie
MOVE WnumMaladie TO fmal_num
READ fmaladies
INVALID KEY
      DISPLAY"Cette maladie n'existe pas"
NOT INVALID KEY
       *>Se Positionner
               MOVE WnumMaladie TO fpat_maladieNum
               START fpatients, KEY IS=fpat_maladieNum
               INVALID KEY
                   DISPLAY"Aucun malade n'a cette maladie  "
               NOT INVALID KEY
                   MOVE 0 TO Wfin2
                   MOVE 0 TO nbMalades
                   PERFORM WITH TEST AFTER UNTIL Wfin2 = 1 OR fpat_maladieNum > fmal_num OR fpat_maladieNum < fmal_num
                   *> Lire Suiv
                       READ fpatients NEXT
                       AT END
                           MOVE 1 TO Wfin2
                       NOT AT END
                           ADD 1 TO nbMalades
                        END-READ
                    END-PERFORM
               
               DISPLAY "La maladie ", fmal_nom, " touche ", nbMalades, " patients."
       
END-IF

CLOSE fmaladies
CLOSE fpatients.



*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Permet d'afficher les information des anciens patients
AFFICHER_PATIENTS_SORTANTS.
OPEN INPUT fpatientsSortants
OPEN INPUT fmaladies
MOVE 0 TO Wfin

PERFORM WITH TEST AFTER UNTIL Wfin = 1
       READ fpatientsSortants
       AT END MOVE 1 To Wfin
       NOT AT END
           *> Lecture directe sur la maladie
           MOVE fpartsort_maladieNum TO fmal_num
           READ fmaladies
           INVALID KEY
               *> Impossible car si un patient est sortant il avait forcément une maladie
               DISPLAY "Maladie non trouvée"
           NOT INVALID KEY
               DISPLAY "Patient n°", fpatsort_num, " est ", fpatsort_etat, " de la maladie suivante : ", fmal_nom
           END-READ
        END-READ
END-PERFORM

CLOSE fpatientsSortants.

PATIENT_TRANSFERE.

OPEN I-O fpatients

MOVE WidPatient TO fpat_id

READ fpatients
INVALID KEY 
       DISPLAY "Pas de patient"
NOT INVALID KEY
       DELETE fpatients 
END-READ

CLOSE fpatients.

SUPPRIMER_PATIENT.
       OPEN I-O fpatients
       DISPLAY fpatients_CR
       DISPLAY 'Donnez l''identifiant du patient à supprimer'
       ACCEPT Wnumero
       MOVE Wnumero TO fpat_id
       READ fpatients
           INVALID KEY
               DISPLAY 'Ce patient n''existe pas'
           NOT INVALID KEY
                PERFORM AFFICHER_UN_PATIENT
                PERFORM WITH TEST AFTER UNTIL Wrep = 0 OR Wrep = 1
                DISPLAY"Ce patient est guéri ou mort ? 0-1"
                ACCEPT Wrep
                END-PERFORM
                MOVE fpat_maladieNum TO WnumMaladie
                OPEN I-O fmaladies
                MOVE WnumMaladie TO fmal_num
                READ fmaladies
                NOT INVALID KEY 
                   *> Patient guéri
                   IF Wrep = 0 THEN
                       MOVE "guéri" TO wEtat
                       ADD 1 fmal_nbGueris TO fmal_nbGueris
                       REWRITE maladie_tamp
                   *> Patient mort
                   ELSE
                       MOVE "mort" TO wEtat
                       ADD 1 fmal_nbMorts TO fmal_nbMorts
                       REWRITE maladie_tamp
                   END-IF  
                CLOSE fmaladies

                OPEN EXTEND fpatientsSortants
                IF fpatientsSortants_CR = 35 THEN  
                   OPEN OUTPUT fpatientsSortants
                END-IF

                MOVE fpat_id TO fpatsort_num
                MOVE wEtat TO fpatsort_etat
                MOVE fpat_maladieNum TO fpartsort_maladieNum
                WRITE patientsSortants_tamp
                CLOSE fpatientsSortants
                PERFORM MAJ_PLACELIBRES2     
               DELETE fpatients RECORD
       END-READ
       CLOSE fpatients.

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Permet de supprimer un docteur de fdocteurs
SUPPRIMER_DOCTEUR.
       OPEN I-O fdocteurs
       DISPLAY fdocteurs_CR
       DISPLAY 'Donnez l''identifiant du docteur à supprimer'
       ACCEPT fdoc_id
       READ fdocteurs
           INVALID KEY
               DISPLAY 'Ce docteur n''existe pas'
           NOT INVALID KEY
               PERFORM AFFICHER_UN_DOCTEUR
               MOVE fdoc_id TO Wnumero
               DISPLAY "Avant de supprimer ce docteur, il va falloir ré-attribuer tous ses patients"
               PERFORM LECTURE_ZONE_PATIENTS_DOCTEUR
               DISPLAY "HERE 8"
               MOVE Wnumero TO fdoc_id
               DISPLAY fdoc_id
               DELETE fdocteurs RECORD
       END-READ
       CLOSE fdocteurs.

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Permet de supprimer un batiment de fbatiment 
SUPPRIMER_BATIMENT.
OPEN I-O fbatiments
*> si batiment vide : le supprimer 
*> si batiment avec patients : regarder si un autre batiment prend en charge cette maladie et si il est peu accueillir 
*> si batiment avec patients : si aucuns batiments ne peut prendre en charge les patienst alors regarder places libres pour tous les mettre dans le même batiment 

DISPLAY 'Quel est le numéro du batiment à supprimer ?'
ACCEPT WnumBat
*> Lecture direct pour récupérer les informations sur le batiment 
MOVE WnumBat TO fbat_num

READ fbatiments
INVALID KEY 
    DISPLAY 'Ce batiment n existe pas.'
NOT INVALID KEY
    *> Compte le nombre de patient dans le batiment 
    *> On fait la différence entre la capacité d'accueil et le nombre de places de libres  

    *>SUBSTRACT fbat_nbPlacesLibres FROM fbat_capacite GIVING WnbrPatient 
    COMPUTE WnbrPatient = fbat_capacite - fbat_nbPlacesLibres
    DISPLAY 'Le batiment que vous voulez supprimer possède ',WnbrPatient, ' patient(s).'
END-READ 

*> Stockage du numéro de la maladie 
MOVE fbat_maladieNum TO WmaladieBat
DISPLAY 'On cherche un batiment qui puisse accueillir la maladie : ',WmaladieBat

*> Si il y a des patients dans le batiment 
IF WnbrPatient > 0 THEN 
*> si le compteur WnbrPatient est différent de 0 
        *> Lecture séquentiel sur batiment pour identifier un batiment qui prend en charge la maladie et qui a une capacité d'accueil
        MOVE 0 TO WnewBatPatient
        READ fbatiments NEXT
        AT END
            DISPLAY 'Aucun batiment peut accueillir les patients'
            *> Si on ne trouve pas de batiment qui correspond à la maladie et qui est libre 
        NOT AT END
            *> recherche si un batiment peut accueillir les patients 
            IF fbat_maladieNum = WmaladieBat THEN
                IF fbat_nbPlacesLibres >= WnbrPatient THEN 
                    
                    MOVE fbat_num TO WnewBatPatient
                    DISPLAY 'On a trouvé un batiment', WnewBatPatient
                END-IF
            END-IF
        END-READ

        *> Vérification si il y a un batiment qui correspond ou pas
        MOVE 0 TO WoptBat 
        IF WnewBatPatient IS ZERO THEN 
            DISPLAY'Il n y a pas de batiment disponible pour accueillir les patients présents dans le batiment supprimé.'
            PERFORM WITH TEST AFTER UNTIL WoptBat = 1 OR WoptBat = 2
                DISPLAY' 2 options : 1) Créer un nouveau batiment    2) Appeler une autre zone de quarantaine '
                ACCEPT WoptBat
            END-PERFORM

            IF WoptBat = 1 THEN 
                *> Il doit ajouter un batiment
                CLOSE fbatiments
                PERFORM AJOUTER_BATIMENT
                OPEN I-O fbatiments
                *> Puis il faut réécrire les patients avec le nouveau numéro du batiment
            ELSE
                *> Les patients sont transférés vers une autre zone de quarantaine 
                *> Ils vont disparaitre et aller dans le fichier historique en temps que transféré 
                    OPEN INPUT fpatients
                    MOVE WnumBat TO fpat_batimentNum

                    DISPLAY 'Numero du batiment ', fpat_batimentNum
                    START fpatients, KEY IS = fpat_batimentNum
             
                    INVALID KEY 
                        DISPLAY 'Le transfert des patients a échoué'
                    NOT INVALID KEY 
                        DISPLAY 'On a trouvé des patients à transférer '
                        *> Si on trouve un patient présent dans ce batiment
                        *> Alors on le mets dans l'historique 
                    END-START
                    MOVE 0 TO finZone
                    PERFORM WITH TEST AFTER UNTIL finZone = 1 AND fpat_batimentNum IS NOT EQUAL TO WnumBat 
                        READ fpatients NEXT 
                        AT END 
                            MOVE 1 TO finZone
                        NOT AT END
                            MOVE fpat_id TO WidPatient
                            *> Appel pour historisé les patients 
                            CLOSE fpatients
                            PERFORM PATIENT_TRANSFERER 
                            OPEN INPUT fpatients 
                        END-READ
                    END-PERFORM
                    CLOSE fpatients
            END-IF    
        END-IF

        IF WoptBat = 0 OR WoptBat = 1 THEN
        *> La modification prend en charge le batiment trouvé ou le batiment créé 
            *> Modifier le fichier patients pour réecrire le nouveau numéro batiment 
            OPEN I-O fpatients
            MOVE WnumBat TO fpat_batimentNum

            DISPLAY 'Numero du batiment ', fpat_batimentNum
            START fpatients, KEY IS = fpat_batimentNum
             
            INVALID KEY 
                DISPLAY 'La modification de batiment des patients a échoué. Désolé veuillez le faire manuellement. '
            NOT INVALID KEY 
                DISPLAY 'On a trouvé des patients '
                *> Si on trouve un patient présent dans ce batiment
                *> Alors on fait la modification du batiment du patient 
            END-START
            MOVE 0 TO finZone
            PERFORM WITH TEST AFTER UNTIL finZone = 1 AND fpat_batimentNum IS NOT EQUAL TO WnumBat 
                READ fpatients NEXT 
                AT END 
                    MOVE 1 TO finZone
                NOT AT END
                    *> Ecriture du tampon vers le fichier patient 
                    MOVE WnewBatPatient TO fpat_batimentNum
                    REWRITE patient_tamp 
                END-READ
            END-PERFORM
            CLOSE fpatients
        END-IF
END-IF
 
MOVE WnumBat TO fbat_num

*> Supprime le batiment maintenant qu'il n'a pas de patient 
READ fbatiments
INVALID KEY 
    DISPLAY 'Le batiment n a pas été supprimer '
NOT INVALID KEY 
   DELETE fbatiments RECORD
   DISPLAY 'Le batiment a bien été supprimé et les patients réintégrés dans un batiment'
END-READ

CLOSE fbatiments.

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Permet de supprimer une maladie
SUPPRIMER_MALADIE.


*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Permet de modifier un patient
MODIFIER_PATIENT.
       IF fpatients_CR <> 00 THEN
           OPEN I-O fpatients
           DISPLAY 'Donnez l''identifiant du patient à modifier'
           ACCEPT fpat_id
       END-IF
       READ fpatients
           INVALID KEY
               DISPLAY 'Ce patient n''existe pas'
           NOT INVALID KEY
               MOVE 0 TO min
               MOVE 7 TO max
               MOVE 8 TO Wchoix
               PERFORM WITH TEST AFTER UNTIL Wchoix = 0
                   
                   PERFORM AFFICHER_UN_PATIENT
                   DISPLAY '****Menu modification du patient****'
                   DISPLAY "1. Nom / 2. Prenom / 3. Docteur / 4. Maladie / 5. Batiment / 6. Etat du patient / 7. Chambre / 0. Quitter"
                   PERFORM SAISIR_ENTRE
                   DISPLAY "Votre choix est le ", Wchoix
                       
                   IF Wchoix = 1 THEN
                       DISPLAY 'Nom du patient:'
                       ACCEPT fpat_nom
                   ELSE IF Wchoix = 2 THEN
                           DISPLAY 'Prénom du patient : '
                           ACCEPT fpat_prenom
                       ELSE IF Wchoix = 3 THEN
                               *>Modification sécurisé du médecin  
                               PERFORM VERIFIER_FPAT_DOCTEURNUM
                           ELSE IF Wchoix = 4 THEN
                                   *>Modification sécurisé de la maladie 
                                   PERFORM VERIFIER_FPAT_MALADIENUM
                                   DISPLAY fpat_batimentNum
                                       MOVE fpat_batimentNum TO WnumBat
                                       PERFORM VERIFIER_FPAT_FBATNUM
                                       IF fpat_batimentNum <> WnumBat THEN                      *> Si il y a eu changement du champ
                                           MOVE WnumBat TO fbat_num
                                           PERFORM MAJ_PLACELIBRES2
                                       END-IF
                               ELSE IF Wchoix = 5 THEN
                                       *>Modification sécurisé du batiment 
                                       DISPLAY fpat_batimentNum
                                       MOVE fpat_batimentNum TO WnumBat
                                       PERFORM VERIFIER_FPAT_FBATNUM
                                       IF fpat_batimentNum <> WnumBat THEN                      *> Si il y a eu changement du champ
                                           MOVE WnumBat TO fbat_num
                                           PERFORM MAJ_PLACELIBRES2
                                       END-IF
                                   ELSE IF Wchoix = 6 THEN
                                           DISPLAY 'Niveau d''avancement de la maladie :'
                                           ACCEPT fpat_niveau
                                       ELSE IF Wchoix = 7 THEN
                                               DISPLAY 'Numéro de la chambre :'
                                               ACCEPT fpat_chambre
                                            END-IF
                                       END-IF
                                   END-IF
                               END-IF
                           END-IF
                       END-IF
                       REWRITE patient_tamp 
                   END-IF         
               END-PERFORM
       END-READ
       CLOSE fpatients
.       

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Permet de modifier un docteur
MODIFIER_DOCTEUR.
       OPEN I-O fdocteurs
       DISPLAY fdocteurs_CR
       DISPLAY 'Donnez l''identifiant du patient à modifier'
       ACCEPT Wnumero
       MOVE Wnumero TO fdoc_id
       READ fdocteurs
           INVALID KEY
               DISPLAY 'Ce docteur n''existe pas'
           NOT INVALID KEY
               MOVE 0 TO min
               MOVE 4 TO max
               PERFORM WITH TEST AFTER UNTIL Wchoix = 0
                   PERFORM AFFICHER_UN_DOCTEUR
                   DISPLAY '****Menu modification du docteur****'
                       DISPLAY "1. Nom / 2. Prenom / 3. Secteur / 4. Rang / 0. Quitter"
                       PERFORM SAISIR_ENTRE
                       DISPLAY "Votre choix est le ", Wchoix

                       IF Wchoix = 1 THEN 
                           DISPLAY 'Nom du docteur:'
                           ACCEPT fdoc_nom
                       ELSE IF Wchoix = 2 THEN
                                DISPLAY 'Prénom du docteur : '
                                ACCEPT fdoc_prenom
                           ELSE IF Wchoix = 3 THEN
                                   DISPLAY 'Precisez le secteur d''expertise du docteur'
                                   ACCEPT fdoc_secteur
                               ELSE IF Wchoix = 4 THEN
                                       DISPLAY 'Donnez le rang de reponsabilité du docteur'
                                       ACCEPT fdoc_rangResp
                                   END-IF
                               END-IF
                           END-IF
                           REWRITE docteur_tamp 
                       END-IF
                   END-PERFORM                  
       END-READ
       CLOSE fdocteurs.

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Permet de modifier un batiement
MODIFIER_BATIMENT.
    OPEN I-O fbatiments
    MOVE 1 TO WbatExiste
    PERFORM WITH TEST AFTER UNTIL WbatExiste = 0

       DISPLAY "Saisissez le numéro du batiment que vous voulez modifier."
       ACCEPT WnumBat
       
       *> Lecture direct car le num du batiment est clé principale 
       MOVE WnumBat TO fbat_num
       
       READ fbatiments
           INVALID KEY 
               DISPLAY "Ce batiment n'existe pas"

           NOT INVALID KEY
               MOVE 0 TO WbatExiste 
               *> Affichage des anciens attributs 
               DISPLAY 'Voici les informations sur le batiment que vous voulez modifier. '
               DISPLAY 'Numéro du batiment : ', fbat_num
               DISPLAY 'Numéro du responsable : ', fbat_responsable
               DISPLAY 'Numéro de la maladie prise en charge : ', fbat_maladieNum
               DISPLAY 'Capacité d accueil : ', fbat_capacite
               DISPLAY 'Nombre de places de libres : ', fbat_nbPlacesLibres
               DISPLAY 'Grade inférieur d accès au batiment : ', fbat_droitAcces
               DISPLAY '-----------------'
       
               *> Saisie des nouveaux attributs     
               *> Saisie du docteur responsable du batiment 
                MOVE 1 TO WdocValide
                PERFORM WITH TEST AFTER UNTIL WdocValide = 0  
                *> Vérification sur la saisie 
                *> Il faut aussi vérifier que le docteur est un rang supérieur à 7 pour être responsable
                    DISPLAY 'Identifiant du docteurs responsable du batiment (attention le rang dans être supérieur à 7):'
                    ACCEPT WdocExiste
                OPEN INPUT fdocteurs
                MOVE WdocExiste TO fdoc_id

                READ fdocteurs
                INVALID KEY
                    DISPLAY 'Veuillez de nouveau saisir le numéro du docteur parmie la liste suivante : '
                    CLOSE fdocteurs
                    PERFORM LISTE_RANG_SEPT
                    OPEN INPUT fdocteurs
                NOT INVALID KEY
                    IF fdoc_rangResp > 6 THEN 
                        MOVE 0 TO WdocValide
                        MOVE WdocExiste TO fbat_responsable
                    ELSE 
                        DISPLAY 'Le rang de ce docteur n est pas accès élevé (rang 7 minimum).'
                        CLOSE fdocteurs
                        PERFORM LISTE_RANG_SEPT
                        OPEN INPUT fdocteurs
                        DISPLAY 'Veuilez saisir un identifiant de docteur présent dans la liste '
                END-READ
                CLOSE fdocteurs
            END-PERFORM
            *> On considère que quand un batiment est créer il n'y a pas encore de patient à l'intérieur  
            DISPLAY 'Capacité d accueil du batiment (nb de places) :'
            ACCEPT fbat_capacite

            *> Saisie du nombre de places libres dans le batiment
            *> Vérification que le nombre de places libres ne soit pas sup à la capacité  
            PERFORM WITH TEST AFTER UNTIL fbat_capacite > fbat_nbPlacesLibres OR fbat_capacite = fbat_nbPlacesLibres
                DISPLAY 'Nombre de places de libres dans le batiment (attention elles ne doivent pas être supérieur à la capacité d accueil): '
                ACCEPT fbat_nbPlacesLibres
            END-PERFORM

            *>  Saisie de la maladie et Vérification
            *> Fonction qui valide la saisie de la maladie
            PERFORM VERIFIER_FPAT_MALADIENUM
            *> MOVE TO fbat_maladieNum

            DISPLAY 'Rang inférieur du docteur qui a accès au batiment : '
            *> Vérification que le rang soit bien compris entre le rang minimum et maximum (0 et 9) 
            MOVE 0 TO min
            MOVE 9 TO max
            PERFORM SAISIR_ENTRE
            MOVE Wchoix TO fbat_droitAcces

      
       END-READ
           *> Réecriture du tampon vers le fichier batiment 
           REWRITE batiment_tamp
    END-PERFORM 
       
CLOSE fbatiments.

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Permet de modifier une maladie
MODIFIER_MALADIE.
       *>OPEN I-O fmaladies

       *>DISPLAY "Quelle maladie voulez-vous modifier ?"
       *>ACCEPT WnumMaladie
       
       *>MOVE 0 TO Wfin
       *>MOVE 0 TO Wtrouve
       
       
       *>PERFORM WITH TEST AFTER UNTIL Wfin = 1 OR Wtrouve = 1
               *>READ fmaladies
               *>AT END 
                   *>MOVE 1 TO Wfin
               *>NOT AT END
                   *>IF fmal_num = WnumMaladie THEN 
                      *> MOVE 1 TO Wtrouve
                       *>DISPLAY 'Donnez les informations relatives à la maladie'
                       *>DISPLAY 'Numéro de la maladie :'
                       *>ACCEPT fmal_num
                       *>DISPLAY 'Nom de la maladie : '
                       *>ACCEPT fmal_nom
                       *>DISPLAY 'Taux de mortalité (en %) : '
                       *>ACCEPT fmal_tauxM
                       *>DISPLAY 'Taux de transmission :'
                       *>ACCEPT fmal_tauxT
                       *>WRITE maladie_tamp END-WRITE
                  
       *>END-PERFORM
       *>CLOSE fmaladies.
       
*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Enleve 1 place au nombre de place libre du batiments lorsqu'un patient y est ajouté 
MAJ_PLACELIBRES. *> Va decrementer fbat_nbPlaceLibre de fbat_num
       OPEN I-O fbatiments
       DISPLAY fbat_num
       READ fbatiments
           INVALID KEY
               DISPLAY "Ce batiment n'existe pas"
           NOT INVALID KEY
               DISPLAY "Le nombre de place libre du batiment",fbat_num ,"à été mis à jour"
               MOVE fbat_nbPlacesLibres TO Wnumero
               SUBTRACT 1 FROM Wnumero
               MOVE Wnumero TO fbat_nbPlacesLibres
               DISPLAY "Il reste ",fbat_nbPlacesLibres, " places de libres sur une capacité maximun de ",fbat_capacite
               REWRITE batiment_tamp
        END-READ
       CLOSE fbatiments
.
*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Ajoute 1 place au nombre de placelibre du batimentlorsqu'un patient y est enlevé
MAJ_PLACELIBRES2.
       OPEN I-O fbatiments
       DISPLAY fbat_num
       READ fbatiments
           INVALID KEY
               DISPLAY "Ce batiment n'existe pas"
           NOT INVALID KEY
               DISPLAY "Le nombre de place libre du batiment",fbat_num ,"à été mis à jour"
               MOVE fbat_nbPlacesLibres TO Wnumero
               ADD 1 TO Wnumero
               MOVE Wnumero TO fbat_nbPlacesLibres
               DISPLAY "Il reste ",fbat_nbPlacesLibres, " places de libres sur une capacité maximun de ",fbat_capacite
               REWRITE batiment_tamp
        END-READ
       CLOSE fbatiments
.

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Lors de la création ou modification d'un patient : Verifie que le numéro du docteur renseigné est existant
VERIFIER_FPAT_DOCTEURNUM.
       DISPLAY '******Liste des Docteurs******'
       DISPLAY '-------------'
       PERFORM AFFICHER_DOCTEURS
       OPEN INPUT fdocteurs
       IF fdocteurs_CR <> 00 THEN
           MOVE 1 TO filou
           OPEN INPUT fdocteurs
       ELSE
           PERFORM WITH TEST AFTER UNTIL trouve = 1 
               IF fdocteurs_CR = 35 THEN
                   DISPLAY "****** Creation du fichier docteur ******"
                   MOVE 1 TO Wbool
               ELSE
                   DISPLAY 'Numéro du médecin traitant :'
                   ACCEPT fdoc_id
                   READ fdocteurs RECORD KEY IS fdoc_id
                       INVALID KEY
                           DISPLAY "Ce docteur n'existe pas"
                           MOVE 0 TO trouve
                           DISPLAY "Voulez vous créer un nouveau médecin ? 0/1"
                           ACCEPT Wbool
                       NOT INVALID KEY
                           DISPLAY 'Très bien le médecin à été trouvé'
                           MOVE fdoc_id TO fpat_docteurNum
                           MOVE fdoc_id  TO WnumMedecin
                           MOVE 1 TO trouve
                           DISPLAY "Booléen" Wbool
                           MOVE 0 TO Wbool
                   END-READ
               END-IF
                   
               IF Wbool = 1 THEN
                   CLOSE fdocteurs
                   PERFORM AJOUTER_DOCTEUR
                   DISPLAY "****** Retour à la création d'un patient ******"
                   OPEN INPUT fdocteurs
                ELSE IF Wbool = 0 THEN 
                    CLOSE fdocteurs
                    DISPLAY "Affichage des docteurs :"
                    PERFORM AFFICHER_DOCTEURS
                    OPEN INPUT fdocteurs
               END-IF
               MOVE 0 TO Wbool
           END-PERFORM
               MOVE 0 TO trouve
               IF filou = 1 THEN
                   CLOSE fdocteurs
                   MOVE 0 TO filou
               END-IF
.

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Lors de la création ou de la modification d'un patient : Verifie que la maladie renseigné est existante
VERIFIER_FPAT_MALADIENUM.
       DISPLAY '******Liste des Maladies******'
       DISPLAY '-------------'
       *>MOVE 1 TO WafficherToutesMaladies
       *>PERFORM AFFICHER_MALADIES
       OPEN INPUT fmaladies
               PERFORM WITH TEST AFTER UNTIL trouve = 1 
                   IF fmaladies_CR = 35 THEN
                       DISPLAY "****** Creation du fichier de maladies ******"
                       MOVE 1 TO Wbool
                   ELSE
                       DISPLAY "***** Retour à la création d'un patient *****"
                       DISPLAY 'Numéro de la maladie :'
                       ACCEPT fmal_num
                       READ fmaladies RECORD KEY IS fmal_num
                           INVALID KEY
                               DISPLAY "Cette maladie n'existe pas"
                               MOVE 0 TO trouve
                               DISPLAY "Voulez vous repertorier une nouvelle maladie ? 0/1"
                               ACCEPT Wbool
                                   
                           NOT INVALID KEY
                               DISPLAY 'Très bien la maladie à été trouvé'
                               MOVE fmal_num TO fpat_maladieNum
                               MOVE fmal_num TO WnumMaladie
                               MOVE 1 TO trouve
                               MOVE 0 TO Wbool
                       END-READ
                   END-IF
                   IF Wbool = 1 THEN
                       CLOSE fmaladies
                       PERFORM AJOUTER_MALADIE
                       DISPLAY "****** Retour à la création d'un patient ******"
                       OPEN INPUT fmaladies
                   END-IF
                   MOVE 0 TO Wbool
               END-PERFORM
               MOVE 0 TO trouve
               CLOSE fmaladies
.

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Lors de la création ou de la modification d'un patient : Vérifie que batiment renseigné est existant et qu'il guérit la maladie du patient
VERIFIER_FPAT_FBATNUM.
       DISPLAY '******Liste des Batiments******'
       DISPLAY '-------------'
       PERFORM AFFICHER_BATIMENTS
        OPEN INPUT fbatiments
               PERFORM WITH TEST AFTER UNTIL trouve = 1
                   IF fbatiments_CR = 35 THEN
                       DISPLAY "****** Creation du fichier de batiment ******"
                       MOVE 1 TO Wbool
                   ELSE
                       DISPLAY 'Numéro du batiment :'
                       ACCEPT fbat_num
                       READ fbatiments RECORD KEY IS fbat_num
                           INVALID KEY
                               DISPLAY "Ce batiment n'existe pas"
                               MOVE 0 TO trouve
                               DISPLAY "Voulez vous creer un nouveau batiment ? 0/1"
                               ACCEPT Wbool
                           NOT INVALID KEY
                               IF fpat_maladieNum =fbat_maladieNum THEN
                                   IF fbat_nbPlacesLibres = 0 THEN
                                       DISPLAY 'Ce batiment existe mais il est déjà rempli'
                                       DISPLAY "Voulez vous creer un nouveau batiment ? 0/1"
                                       MOVE 0 TO trouve
                                       ACCEPT Wbool
                                   ELSE 
                                       MOVE fbat_num TO fpat_batimentNum
                                       MOVE fbat_num TO WnumBat
                                       CLOSE fbatiments
                                       PERFORM MAJ_PLACELIBRES
                                       OPEN INPUT fbatiments
                                       MOVE 1 TO trouve
                                       MOVE 0 TO Wbool
                                   END-IF
                               ELSE
                                   DISPLAY "Maladie du patient", fpat_maladieNum
                                   DISPLAY "Maladie du batiment", fbat_maladieNum
                                   DISPLAY "Le batiment ne guérit pas cette maladie"
                                   DISPLAY "Voulez vous creer un nouveau batiment ? 0/1"
                                   ACCEPT Wbool
                               END-IF
                       END-READ
                   END-IF
                   IF Wbool = 1 THEN
                       DISPLAY "*** Création de batiment ***"
                       CLOSE fbatiments
                       PERFORM AJOUTER_BATIMENT
                       DISPLAY "*** Fin de création de batiment ***"
                       DISPLAY "****** Retour sur le patient ******"
                       OPEN INPUT fbatiments
                   END-IF
               END-PERFORM
               MOVE 0 TO trouve
               CLOSE fbatiments
                  
.

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Permet de forcer l'utilisateur a choisir un nombre entre les valeurs min et max
SAISIR_ENTRE. *>UTILISATION : il faut définir un min et un max avant appel
       PERFORM WITH TEST AFTER UNTIL ( ( Wchoix <= max ) AND ( Wchoix >= min ) ) 
           ACCEPT Wchoix
       END-PERFORM
.

*>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
*>> Permet de parcourir tous les patients d'un docteur
LECTURE_ZONE_PATIENTS_DOCTEUR. *>UTILISATION : fdoc_id doit etre renseigner avant l'appel
       OPEN I-O fpatients
           MOVE 0 TO finZone
           MOVE fdoc_id TO fpat_docteurNum
           START fpatients, KEY IS = fpat_docteurNum
               INVALID KEY
                   DISPLAY "Le médecin n'a pas de patient actuellement"
               NOT INVALID KEY
                   READ fpatients NEXT
                       AT END
                           MOVE 1 TO finZone
                       NOT AT END
                           PERFORM MODIFIER_PATIENT
                   END-READ
           END-START
       CLOSE fpatients
.

*> Affichage des docteurs qui ont un rang supérieur à 7 
*> Donc liste des docteurs qui peuvent être responsable
LISTE_RANG_SEPT.
    OPEN INPUT fdocteurs
    DISPLAY fdocteurs_CR
    *> Pour savoir si le fichier est fini (plus aucune lecture possible)    
    MOVE 0 TO Wfin
    PERFORM WITH TEST AFTER UNTIL Wfin = 1
        READ fdocteurs NEXT
            AT END 
                MOVE 1 TO Wfin
            NOT AT END
                IF fdoc_rangResp > 6 THEN
                    DISPLAY 'Numéro : ', fdoc_id
                    DISPLAY 'Nom : ', fdoc_nom
                    DISPLAY 'Prénom : ', fdoc_prenom
                    DISPLAY 'Secteur : ', fdoc_secteur
                    DISPLAY 'Rang : ', fdoc_rangResp
                    DISPLAY '-----------------'
                END-IF
        END-READ
    END-PERFORM
CLOSE fdocteurs.


PATIENT_TRANSFERER.

